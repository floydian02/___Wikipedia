import twitter, urllib.request, json, time, sys, os.path
from apscheduler.schedulers.blocking import BlockingScheduler

sched = BlockingScheduler()

# URLs to use wikipedia's API
random_url = "https://en.wikipedia.org/w/api.php?action=query&list=random&rnnamespace=0&format=json"
summary_url = "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&pageids="
url_url = "https://en.wikipedia.org/w/api.php?action=query&prop=info&inprop=url&format=json&pageids="

# Template for tweet
tweet_template = '''
{} ({})

#ThankYouWikipedia
'''

# Read the config file
configfile = open("config", "r")
config = configfile.readlines()

for ln in config:
    if len(ln.split("=")) != 2:
        print("Error in config file - there were an incorrect amount of things in this line:")
        print("\t" + ln)
        print("Lines must be in the format:")
        print("\t<key>=<value>")
        print("Exiting.")
        sys.exit()

config = [s.split("=")[1].rstrip() for s in config]

# Amount of minutes for which to sleep
try:
    POST_DELAY_MINUTES = float(config[4])
except ValueError:
    print("The value of post_delay_in_minutes must be an INTEGER value!")
    print("\t'" + config[4] + "' was supplied.")
    print("Exiting.")
    sys.exit()

# Make sure used_ids exists and is empty
f = open("used_ids", "w+")
f.close()
    
# To access twitter
api = twitter.Api(
    consumer_key        =config[0],
    consumer_secret     =config[1],
    access_token_key    =config[2],
    access_token_secret =config[3]
)

# Return a json object from a url. The url must link to a json file.
def json_from_url(url):
    json_bytes = urllib.request.urlopen(urllib.request.Request(url)).read()
    json_decoded = json_bytes.decode("utf-8")
    return json.loads(json_decoded)

# Checks if a particular id has already been used
def has_id_been_used(id):
    return json_from_url(str(config[5])+"?key="+str(id))["exists"]

# Confirms usage of an id by writing it to a file
def use_id(id):
    used_ids = open("used_ids", "a")
    used_ids.write(str(id) + "\n")
    used_ids.close()

@sched.scheduled_job('interval', minutes=POST_DELAY_MINUTES)
def dopost(dummy=False):
    '''
    Make the actual post - randomly selects a page using the wikipedia api directly by querying some data,
    then uses the wikipedia api module I'm importing to get the url from the page name.
    Then, it constructs a string to post to twitter, in the format:

      <Link to the wikipedia page>
      <The summary of the wikipedia page, but cropped to fit a tweet with four characters left> ...

    Use dummy=True to do everything BUT actually tweeting it, for testing.
    '''
    # Query the wikipedia api to get the id of a random page, and the summary paragraph of that page.
    page_id = -1
    
    while (page_id < 0 or has_id_been_used(page_id)):
        page_id = json_from_url(random_url)["query"]["random"][0]["id"]
        print("Trying page_id = " + str(page_id))

    # Confirm this id
    use_id(page_id)

    # Get a few variables
    title = json_from_url(summary_url + str(page_id))["query"]["pages"][str(page_id)]["title"].rstrip()
    url = json_from_url(url_url + str(page_id))["query"]["pages"][str(page_id)]["fullurl"]
    
    tweet_content = tweet_template.format(title, url).rstrip()[:280]

    # Send the actual post, if not a dummy run.
    if (not dummy):
        try:
            api.PostUpdate(tweet_content)
        except twitter.error.TwitterError:
            print("Could not authenticate this twitter user. Double check your keys.")
            print("Exiting")
            sys.exit()
    else:
        # Otherwise, just print what would've been tweeted!
        print(tweet_content)

    # Visual confirmation, could replace this with perhaps a write to a log file, if wanted!
    print("Post good")
    
if __name__ == "__main__":
    sched.start()
